# DEI Project Statement

<!---
The DEI.md file was originally created in the CHAOSS project. This comment provides attribution of that work as defined under the MIT license
-->

Kali Linux

![](kali-logo-dragon-blue-white.png)

This DEI.md file covers the entirety of the Kali Linux GitLab group and any included projects. Our intent is to ensure that our commitment to running an open project that welcomes contributions from everyone regardless of backgroud is made clear.

Our project prioritizes and reflects on DEI through a regular review of project policies and performance. This reflection is documented, in part, in the following DEI.md file based on specific [CHAOSS](https://chaoss.community) DEI metrics.  

### [Project Access](https://chaoss.community/?p=4953)

Project access is addressed in our project through various efforts. Through these efforts, we aim to support access for all. Specific efforts in our project include:

- A primary goal with Kali Linux is to encourage and welcome any contributions for any project, and actively participate in many events that encourage contributions.
- Kali Linux strives to have as many projects publicly accessible as possible.
- There are many available ways for contributors to communicate with the Kali Linux team that we monitor.

### [Communication Transparency](https://chaoss.community/?p=4957)

Communication Transparency is addressed in our project through a variety of different efforts. Through these efforts, we aim to support transparency for all. Specific efforts in our project include:

- The Kali Linux team makes an effort to continue discussions with community members on GitLab to promote engagement from other community members.
- Kali Linux makes an effort to operate as much in the public when possible. This includes our documentation, package repository, and various build scripts.
- All Kali Linux documentation is public and open for contribution.
- Regular reviewing of private projects is done to determine what can be public and what needs to be done to make a project public.

### [Newcomer Experiences](https://chaoss.community/?p=4891)

The newcomer experience is addressed in our project through a variety of different efforts. Through these efforts, we aim to support the newcomer experience for all new members. Specific efforts in our project include:

- Kali Linux has extensive documentation regarding various ways to contribute to the project, and helps to guide new contributors.
- The Kali Linux team encourages community members to report feedback or bugs through appropriate channels and provides the necessary material to do so.
- The Kali Linux team regularly invites community members to share their stories or process that they followed to contribute to Kali in meaningful ways.
- Kali Linux uses common language and terminology in order to reach a wider range of users and promote a simpler to understand experience to new community members.
- Kali Linux strives to maintain relationships with recurring contributors and foster new relationships with new contributors.

### [Inclusive Leadership](https://chaoss.community/?p=3522)

Inclusive leadership is addressed in our project through a variety of different efforts. Through these efforts, we aim to support leadership opportunities for project members with an interest. Specific efforts in our project include:

- Ownership of projects is based on output and contributions over time. Anyone in the community can obtain ownership of areas that are interesting to them based on their engagement.

### Other Efforts

Our project is doing other DEI-related efforts or has plans to, that are not mentioned above.

1.  Kali Linux has plans to work on future DEI-related efforts not mentioned above to address all four categories.

We recognize that the inclusion of the DEI.md file and the provided reflection on the specific DEI metrics does not ensure community safety nor community inclusiveness. The inclusion of the DEI.md file signals that we, as a project, are committed to centering DEI in our project and regularly reviewing and reflecting on our project DEI practices.

If you do not feel that the DEI.md file appropriately addresses concerns you have about community safety and inclusiveness, please let us know. You can do this by reporting your concerns to our Code of Conduct team (codeofconduct@kali.org) to raise concerns you have.

Last Reviewed: January 26, 2024